def task1_1(input):
    max_num = 0
    actual_num = 0
    for num in input:
        if num:
            actual_num += num
        else:
            if actual_num > max:
                max_num = actual_num
            actual_num = 0
    print(max_num)

def task1_2(input):
    cals = []
    actual_num = 0
    for num in input:
        if num:
            actual_num += num
        else:
            cals.append(actual_num)
            actual_num = 0
    print(sum(sorted(cals, reverse=True)[:3]))
    

if __name__ == '__main__':
    with open("input.txt") as f:
        input_list = [int(i) if i else None for i in f.read().splitlines()]
        task1_1(input_list)
        task1_2(input_list)



