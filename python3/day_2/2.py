opponent = {
"A": "rock",
"B": "paper",
"C": "scissors"
}

me = {
"X": "rock",
"Y": "paper",
"Z": "scissors"
}

points = {
"AX": 1 + 3,
"BX": 1 + 0,
"CX": 1 + 6,

"AY": 2 + 6,
"BY": 2 + 3,
"CY": 2 + 0,

"AZ": 3 + 0,
"BZ": 3 + 6,
"CZ": 3 + 3,
}

game_points = {
"X": 1,
"Y": 2,
"Z": 3
}

loose = {
    "A": "Z",
    "B": "X",
    "C": "Y"
}

draw = {
    "A": "X",
    "B": "Y",
    "C": "Z"
}

win = {
    "A": "Y",
    "B": "Z",
    "C": "X"
}

def task_one():
    with open("input.txt") as f:
        total_score = 0
        for row in f.readlines():
            # print(f"opponent will play {opponent[row[0]]}")
            # print(f"I should play {me[row[2]]}")
            total_score += points[row[0] + row[2]]
        print("total_score:", total_score)

def task_two():
    with open("input.txt") as f:
        total_score = 0
        for row in f.readlines():
            # print(f"opponent will play {opponent_turn[row[0]]}")
            # print(f"{me_strategy[row[2]]}")
            # print(f"I should play {me[row[2]]}")
            if row[2] == "X":
                total_score += 0 + game_points[loose[row[0]]]
            elif row[2] == "Y":
                # draw
                total_score += 3 + game_points[draw[row[0]]]
            else:
                # win
                total_score += 6 + game_points[win[row[0]]]

        print("total_score:", total_score)


if __name__ == '__main__':
    task_one()
    task_two()
