import string
from itertools import islice

def score_dict():
    dict_score = {}
    for i, j in enumerate(string.ascii_lowercase + string.ascii_uppercase, start=1):
        dict_score[j] = i
    return dict_score

def task_one():
    with open("input.txt") as f:
        duplicates = []
        for row in f.readlines():
            row = row.strip()
            split = len(row) // 2
            a = sorted(row[:split])
            b = sorted(row[split:])
            row_duplicates = set()
            for i in a:
                for j in b:
                    if i == j:
                        row_duplicates.add(i)

            duplicates.extend(row_duplicates)

        dict_score = score_dict()
        total_score = 0
        for i in duplicates:
            total_score += dict_score[i]
        print(total_score)


def task_two():
    with open("input.txt") as f:
        duplicates = []
        # iterate through
        start = 0
        stop = 3
        while True:
            row_duplicates = set()
            head = [x.strip() for x in list(islice(f, 3))]
            if head:
                for i in head[0]:
                    for j in head[1]:
                        for k in head[2]:
                            if i == j == k:
                                row_duplicates.add(i)
                duplicates.extend(row_duplicates)

            else:
                break
        dict_score = score_dict()
        total_score = 0
        for i in duplicates:
            total_score += dict_score[i]
        print(total_score)

if __name__ == '__main__':
    task_one()
    task_two()



