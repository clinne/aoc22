def task_one():
    with open("input.txt") as f:
        total = 0
        for row in f.readlines():
            row = row.strip().split(",")
            a_start = int(row[0].split("-")[0])
            a_end = int(row[0].split("-")[1])
            b_start = int(row[1].split("-")[0])
            b_end = int(row[1].split("-")[1])

            a = list(range(a_start, a_end + 1))
            b = list(range(b_start, b_end + 1))
            if all(item in a for item in b) or all(item in b for item in a):
                total += 1
        print(total)


def task_two():
    with open("input.txt") as f:
        total = 0
        for row in f.readlines():
            row = row.strip().split(",")
            a_start = int(row[0].split("-")[0])
            a_end = int(row[0].split("-")[1])
            b_start = int(row[1].split("-")[0])
            b_end = int(row[1].split("-")[1])

            a = list(range(a_start, a_end + 1))
            b = list(range(b_start, b_end + 1))
            if any(item in a for item in b) or any(item in b for item in a):
                total += 1
        print(total)


if __name__ == '__main__':
    task_one()
    task_two()
